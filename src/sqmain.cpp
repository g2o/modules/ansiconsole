#include <Windows.h>
#include <sqrat.h>

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);

	HANDLE hIn = GetStdHandle(STD_INPUT_HANDLE);
	if (hIn == INVALID_HANDLE_VALUE)
		return sq_throwerror(vm, "(ANSIConsole) failed to get STD_INPUT_HANDLE.");

	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hOut == INVALID_HANDLE_VALUE)
		return sq_throwerror(vm, "(ANSIConsole) failed to get STD_OUTPUT_HANDLE.");

	DWORD dwInMode;
	if (!GetConsoleMode(hOut, &dwInMode))
		return sq_throwerror(vm, "(ANSIConsole) failed to GetConsoleMode for STD_INPUT_HANDLE.");

	DWORD dwOutMode;
	if (!GetConsoleMode(hOut, &dwOutMode))
		return sq_throwerror(vm, "(ANSIConsole) failed to GetConsoleMode for STD_OUTPUT_HANDLE.");

	dwInMode |= ENABLE_VIRTUAL_TERMINAL_INPUT;
	if (!SetConsoleMode(hIn, dwInMode))
		return sq_throwerror(vm, "(ANSIConsole) failed to ENABLE_VIRTUAL_TERMINAL_INPUT for STD_INPUT_HANDLE.");

	dwOutMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
	if (!SetConsoleMode(hOut, dwOutMode))
		return sq_throwerror(vm, "(ANSIConsole) failed to ENABLE_VIRTUAL_TERMINAL_PROCESSING for STD_OUTPUT_HANDLE.");

	return SQ_OK;
}
