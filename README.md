# ANSIConsole

## Table of contents

- [Introduction](#introduction)
- [Usage requirements](#usage-requirements)
- [Installing module](#installing-module)
- [Loading module](#loading-module)
- [Usage example](#usage-example)
- [Build requirements](#build-requirements)
- [Build instructions](#build-instructions)
- [CMake options](#cmake-options)

## Introduction

**ANSIConsole** is a module that enables the feature introduced since **Windows 10 TH2 (v1511)**, which allow to use **ANSI/VT100** escape sequences for `cmd.exe` shell.  
If you're interested in learning more, you can checkout:
- [Microsoft docs](https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences)
- [this article](https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html)

**NOTE!** This module will work only for `cmd.exe` shell, which is the default one on most of the systems.  
**NOTE!** This module will work only from this specific **Windows 10** update, so unfortunately older windows versions aren't supported, e.g:
- Windows XP
- Windows Vista,
- Windows 7
- Windows 8
- Windows 8.1

If you want to have this feature under older version of **Windows**, you can use `git bash` shell as an alternative (simply run G2O Server from git bash),  
just be aware that color prints from g2o modification won't work, because git bash isn't aware of setting console colors that are specific to windows.

This module is **windows specifc** only, because **Linux** natively suports this feature.

## Usage requirements

This package is essential only for **Windows** platform, and it is installed by default by g2o installer. \
In order to use the module, the user have to install: \
[Microsoft Visual C++ 2015-2022 Redistributable (x86)](https://aka.ms/vs/17/release/vc_redist.x86.exe)

## Installing module

**_NOTE:_** Client modules aren't downloaded by default by g2o server.  
You have to put them manually into: `Game/Multiplayer/Modules` directory.

In order to install the module you can either [download the prebuilt binary](../../releases) from releases, or [build the module yourself](#build-instructions).  
Be sure to install the module with matching CPU architecture for your server app.

## Loading module

To load the module, you have to put `<module>` tag into .xml server configuration.  
Below you can find more info about this tag **attributes**.

```xml
<!--The path to the module relative to .xml file-->
<!--Client-side module must be placed in the exact game path from which it is loaded on the server, e.g: -->
<!--Loading module on s-side: server/MyServerName/sqmodule.dll-->
<!--Will cause the module to be searched in: game/Multiplayer/Modules/MyServerName/sqmodule.dll-->
<!--[required]--> src="path/to/the/module"

<!--[required]--> type="client"|"server"

<!--By default module will be loaded without checksum validation-->
<!--useful when you want to load only specific version of the module-->
<!--[optional]--> md5="1a79a4d60de6718e8e5b326e338ae533"

<!--By default every module is required, you can override this by setting required to false-->
<!--Useful for creating optional modules-->
<!--[optional]--> required=true|false
```

Example of loading client module:
```xml
<module src="sqmodule.x86.dll" type="client" />
```

Example of loading server module:
```xml
<module src="sqmodule.x86.dll" type="server" />
```

## Usage example

```js
// This will print *Hello* in yellow color and *World* in blue color, and then reset the console changes to default values
addEventHandler("onInit", function()
{
    print("\x1b[33mHello \x1b[36mWorld\x1b[0m")
})
```


## Build requirements

**_NOTE:_**  Some of the requirements like _IDE_ or _compiler_ are just recommendation

In order to compile the module, you have to meet some \
essential requirements,
depending on the target platform.

### Windows

- Visual Studio, 2015+ (recommended [2019 Community Edition](https://visualstudio.microsoft.com/pl/thank-you-downloading-visual-studio/?sku=Community&rel=16))
    
    Visual Studio Components
    * Windows SDK
    * one of the following toolsets, pick one: v140, v141, v142 (recommended v142)
    * (Optional) CMake Tools for Visual Studio
- [CMake 3.17+](https://cmake.org/download/)

### Linux

- g++ compiler
- [CMake 3.17+](https://cmake.org/download/)

## Build instructions

### Windows

#### Visual Studio with CMake tools

- open a local folder using Visual Studio
- build the project

#### Visual Studio without CMake tools

- open command line in repo-directory
- type ``mkdir build``
- type ``cd build``
- type ``cmake ..``
- open visual studio .sln and compile the project
- alternatively if you want to build from command line instead, \
    type ``cmake --build .``

### Linux

- open terminal in repo-directory
- type ``mkdir build``
- type ``cd build``
- type ``cmake ..``
- type ``cmake --build .``

## CMake options

This project has some configurable options.  

### Cache options

Cache options are stored inside **CMakeCache.txt** inside generated CMake build folder.  
You can set these options, by either manually editing the file, or by using cmake-gui.

- **GAME_PATH** this cache option allows you to setup the destination  
    where module will be installed. If it's set to some value, it will  
    generate an installation step. by default it's set to ``""`` (disabled).

- **SERVER_PATH** this cache option allows you to setup the destination  
    where module will be installed. If it's set to some value, it will  
    generate an installation step. by default it's set to ``""`` (disabled).

- **INSTALL_AFTER_BUILD** this cache option allows you to run  
    cmake install step after a successfull build. By default it's disabled.
